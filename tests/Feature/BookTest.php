<?php

namespace Tests\Feature;

use App\Book;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;
    /**
     * @test storing new book.
     * A basic test example.
     *
     * @return void
     */
    public function storingBook()
    {
        $book = factory(Book::class)->make()->toArray();

        $response = $this->post('/books', $book);

        $this->assertDatabaseHas('books', $book);

        $response->assertSuccessful();
    }
}
