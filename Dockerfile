FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
RUN echo "America/Los_Angeles" > /etc/timezone
RUN apt update
RUN DEBIAN_FRONTEND=noninteractive; apt install -y php7.2-mbstring php7.2-xml php7.2-fpm php7.2-zip composer php7.2-sqlite php7.2-common php7.2-curl nginx
RUN rm -rf /etc/nginx/sites-enabled/*
COPY . /var/www/html
COPY nginx.conf /etc/nginx/sites-enabled/nginx.conf

WORKDIR /var/www/html

RUN composer install --no-dev --optimize-autoloader
RUN cp .env.example .env
RUN php artisan optimize

EXPOSE 8000

CMD php artisan serve
